class Car:
    def __init__(self):
        self.model = {}
    
    def __setitem__(self, key, value):
        self.model[key] = value

    def __getitem__(self, item):
        return self.model[item]



bmw = Car()
bmw['X5'] = 'V8 engine'
bmw['7 series'] = 'V12 engine'
print(bmw['X5'])